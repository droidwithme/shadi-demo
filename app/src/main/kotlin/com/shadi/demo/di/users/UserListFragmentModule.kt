package com.shadi.demo.di.users

import com.shadi.presentation.ui.users.UsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserListFragmentModule {

    @ContributesAndroidInjector
    abstract fun userListFragment(): UsersFragment
}