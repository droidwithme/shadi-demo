package com.shadi.demo.di.users

import com.shadi.data.api.ShadiApi
import com.shadi.data.datasource.userlist.ShadiDatabaseDataSource
import com.shadi.data.datasource.userlist.ShadiDatabaseDataSourceImpl
import com.shadi.data.datasource.userlist.ShadiListApiDataSource
import com.shadi.data.datasource.userlist.ShadiListApiDataSourceImpl
import com.shadi.data.db.userlist.UserListDao
import com.shadi.data.repository.userlist.UserListRepositoryImpl
import com.shadi.domain.repository.users.UsersListRepository
import com.shadi.domain.usecase.users.UserListUseCase
import com.shadi.domain.usecase.users.UserListUseCaseImpl
import com.shadi.presentation.common.transformer.AsyncFTransformer
import com.shadi.presentation.common.transformer.AsyncSTransformer
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executors

/**
 * Created by Droider.
 */
@Module
class UserListModule {

    @Provides
    //@PerFragment
    fun provideDatabaseSource(shadiListDao: UserListDao): ShadiDatabaseDataSource =
        ShadiDatabaseDataSourceImpl(shadiListDao, Executors.newSingleThreadExecutor())


    @Provides
    //@PerFragment
    fun provideShadiApiSource(api: ShadiApi): ShadiListApiDataSource =
        ShadiListApiDataSourceImpl(api)

    @Provides
    //@PerFragment
    fun provideRepository(
        apiDataSource: ShadiApi,
        shadiListApiDataSource: ShadiListApiDataSource,
        shadiDatabaseDataSource: ShadiDatabaseDataSource
    ): UsersListRepository {
        return UserListRepositoryImpl(
            apiDataSource,
            shadiListApiDataSource,
            shadiDatabaseDataSource
        )
    }

    @Provides
    //@PerFragment
    fun provideUserListUseCaseImpl(repository: UsersListRepository): UserListUseCase =
        UserListUseCaseImpl(
            AsyncFTransformer(),
            AsyncSTransformer(),
            AsyncSTransformer(),
            repository
        )

}
