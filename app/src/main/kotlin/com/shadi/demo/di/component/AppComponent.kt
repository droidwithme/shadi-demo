package com.shadi.demo.di.component

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import com.shadi.demo.MyApplication
import com.shadi.demo.di.AppModule
import com.shadi.demo.di.MainModule
import com.shadi.demo.di.ViewModelModule
import com.shadi.demo.di.users.UserListModule
import javax.inject.Singleton

/**
 * Created by Droider.
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        AppModule::class,
        MainModule::class,
        UserListModule::class
    ]
)
interface AppComponent : AndroidInjector<MyApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MyApplication>()
}