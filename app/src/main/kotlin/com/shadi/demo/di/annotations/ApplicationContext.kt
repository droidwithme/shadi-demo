package com.shadi.demo.di.annotations

import javax.inject.Qualifier

/**
 * Created by Droider on 9/19/2018.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext