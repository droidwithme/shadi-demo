package com.shadi.demo.di.splash

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.shadi.presentation.ui.splash.SplashFragment

@Module
abstract class SplashFragmentModule {

    @ContributesAndroidInjector
    abstract fun splashFragment(): SplashFragment
}