package com.shadi.demo.di.annotations

import javax.inject.Scope

/**
 * Created by Droider on 9/16/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment