package com.shadi.demo.di

import com.shadi.demo.di.splash.SplashFragmentModule
import com.shadi.demo.di.users.UserListFragmentModule
import com.shadi.presentation.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Droider on 10/17/2018.
 */
@Module(
    includes = [ SplashFragmentModule::class, UserListFragmentModule::class]
)
abstract class MainModule {

    //@PerActivity
    @ContributesAndroidInjector
    abstract fun get(): MainActivity
}