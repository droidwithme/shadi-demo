package com.shadi.demo.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import com.shadi.data.db.MyDatabase
import com.shadi.demo.di.annotations.ApplicationContext
import javax.inject.Singleton

/**
 * Created by Droider on 9/19/2018.
 */
@Module
class DbModule {

    @Singleton
    @Provides
    fun provideMyDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, MyDatabase::class.java, "mydatabase")
            .build()

    @Singleton
    @Provides
    fun provideUserDao(myDatabase: MyDatabase) = myDatabase.userDao()
}