package com.shadi.domain.common.transformer

import io.reactivex.SingleTransformer

/**
 * Created by Droider on 10/15/2018.
 */
/**
 * A transformer to io thread for singles.
 */
abstract class STransformer<T> : SingleTransformer<T, T>