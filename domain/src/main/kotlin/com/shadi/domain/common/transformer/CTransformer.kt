package com.shadi.domain.common.transformer

import io.reactivex.CompletableTransformer

/**
 * Created by Droider on 10/15/2018.
 */
/**
 * A transformer to io thread for completables.
 */
abstract class CTransformer : CompletableTransformer