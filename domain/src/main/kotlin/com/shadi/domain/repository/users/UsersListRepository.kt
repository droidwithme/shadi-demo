package com.shadi.domain.repository.users

import io.reactivex.Single
import com.shadi.domain.common.ResultState
import com.shadi.domain.entity.Entity
import com.shadi.domain.repository.BaseRepository

/**
 * Created by Droider on 9/23/2018.
 */
/**
 * User repository
 */
interface UsersListRepository : BaseRepository {

    /**
     * Perform
     */
    fun getUserList(): Single<ResultState<List<Entity.Result>>>

    fun getOfflineUserList(): Single<ResultState<List<Entity.Result>>>

    fun updateUser(user: Entity.Result): Single<String>
}