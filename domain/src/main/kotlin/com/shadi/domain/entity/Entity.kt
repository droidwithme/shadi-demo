package com.shadi.domain.entity

/**
 * Created by Droider on 10/9/2018.
 */
/**
 * Album entity
 */
sealed class Entity {

    data class Users(
        val results: List<Result>
    ) : Entity()


    data class Result(
        var action: Boolean,
        var cell: String,
        var email: String,
        var gender: String,
        var name: Name,
        var nat: String,
        var phone: String,
        var picture: Picture
    ) : Entity()

    data class Name(
        var first: String,
        var last: String,
        var title: String
    ) : Entity()

    data class Picture(
        var large: String
    ) : Entity()


}