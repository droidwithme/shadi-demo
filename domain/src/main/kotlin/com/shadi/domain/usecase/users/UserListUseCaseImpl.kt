package com.shadi.domain.usecase.users

import androidx.paging.PagedList
import io.reactivex.Single
import com.shadi.domain.common.ResultState
import com.shadi.domain.common.transformer.FTransformer
import com.shadi.domain.common.transformer.STransformer
import com.shadi.domain.entity.Entity
import com.shadi.domain.repository.users.UsersListRepository

/**
 * Created by Droider.
 */
/**
 * Album use case implementation
 */
class UserListUseCaseImpl(
    private val transformerFlowable: FTransformer<ResultState<PagedList<Entity.Users>>>,
    private val transformerSingle: STransformer<ResultState<List<Entity.Result>>>,
    private val transformerSingleList: STransformer<ResultState<PagedList<Entity.Users>>>,
    private val repository: UsersListRepository
) : UserListUseCase {

    override fun getUserList(): Single<ResultState<List<Entity.Result>>> =
        repository.getUserList().compose(transformerSingle)

    override fun getOfflineData(): Single<ResultState<List<Entity.Result>>> =
        repository.getOfflineUserList().compose(transformerSingle)


    override fun updateUser(user: Entity.Result): Single<String> {
        return repository.updateUser(user)
    }
}
