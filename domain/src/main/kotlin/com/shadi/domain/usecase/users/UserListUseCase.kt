package com.shadi.domain.usecase.users

import io.reactivex.Single
import com.shadi.domain.common.ResultState
import com.shadi.domain.entity.Entity
import com.shadi.domain.usecase.BaseUseCase

/**
 * Created by Droider
 */
/**
 * User use case
 *
 */
interface UserListUseCase : BaseUseCase {

    /**
     * Get all of user use case
     */
    fun getUserList(): Single<ResultState<List<Entity.Result>>>

    fun getOfflineData(): Single<ResultState<List<Entity.Result>>>

    fun updateUser(user: Entity.Result): Single<String>


}