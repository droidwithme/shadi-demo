package com.shadi.data.repository.userlist

import android.util.Log
import androidx.paging.PagedList
import com.shadi.data.api.ShadiApi
import com.shadi.data.common.extension.applyIoScheduler
import com.shadi.data.datasource.userlist.ShadiDatabaseDataSource
import com.shadi.data.datasource.userlist.ShadiListApiDataSource
import com.shadi.data.mapper.map
import com.shadi.data.repository.BaseRepositoryImpl
import com.shadi.domain.common.ResultState
import com.shadi.domain.entity.Entity
import com.shadi.domain.repository.users.UsersListRepository
import io.reactivex.Single
import java.lang.Exception

/**
 * Created by Droider on 9/28/2018.
 */
/**
 * User List repository implementation
 */
class UserListRepositoryImpl(
    private val apiSource: ShadiApi,
    private val shadiListApiDataSource: ShadiListApiDataSource,
    private val shadiDatabaseDataSource: ShadiDatabaseDataSource
) : BaseRepositoryImpl<Entity.Users>(), UsersListRepository {
    val TAG = UserListRepositoryImpl::class.simpleName

    override fun getUserList(): Single<ResultState<List<Entity.Result>>> {

        val dataFromSever = shadiListApiDataSource.getShadiProfiles(50)

        shadiDatabaseDataSource.saveUserToDb(dataFromSever)


        return dataFromSever.applyIoScheduler().map { it ->
            ResultState.Success(
                it.results.map {
                    Entity.Result(
                        it.action,
                        it.cell,
                        it.email,
                        it.gender,
                        Entity.Name(it.name.first, it.name.last, it.name.title),
                        it.nat,
                        it.phone,
                        Entity.Picture(it.picture.large)
                    )
                }) as ResultState<List<Entity.Result>>
        }.onErrorReturn {
            ResultState.Error(it, null)
        }
    }

    override fun getOfflineUserList(): Single<ResultState<List<Entity.Result>>> {
        val dataFromDb = shadiDatabaseDataSource.getUsersFromDB()
        return dataFromDb.applyIoScheduler().map { it ->
            ResultState.Success(
                it.map {
                    Entity.Result(
                        it.action,
                        it.cell,
                        it.email,
                        it.gender,
                        Entity.Name(it.name.first, it.name.last, it.name.title),
                        it.nat,
                        it.phone,
                        Entity.Picture(it.picture.picture)
                    )
                }) as ResultState<List<Entity.Result>>
        }.onErrorReturn {
            ResultState.Error(it, null)
        }
    }

    override fun updateUser(user: Entity.Result): Single<String> {
        var response = ""
        try {
            shadiDatabaseDataSource.updateUse(user.map())
            response = "User updated successfully"
            return Single.just(response)
        } catch (e: Exception) {
            response = "Error while updating user ${e.message}"
        }
        return Single.just(response)
    }

}