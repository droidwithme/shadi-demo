package com.shadi.data.api

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ShadiApi {


    @GET("/api/")
    fun getUsers(@Query("results") results: Int): Single<Dto.ShadiUsers>


    sealed class Dto {


        data class ShadiUsers(
            @SerializedName("results") var results: List<Result>
        ) : Dto()

        data class Result(
            var action: Boolean,
            var cell: String,
            var email: String,
            var gender: String,
            var name: Name,
            var nat: String,
            var phone: String,
            var picture: Picture
        ) : Dto()

        data class Name(
            var first: String,
            var last: String,
            var title: String
        ) : Dto()

        data class Picture(
            var large: String
        ) : Dto()


    }
}