package com.shadi.data.mapper

import com.shadi.data.db.userlist.UserListData
import com.shadi.domain.entity.Entity

/**
 * Created by Droider on 10/10/2018.
 */
/**
 * Extension class to map
 */
fun Entity.Result.map() = UserListData.UserData(
    0,
    action,
    cell,
    email,
    gender,
    UserListData.Name(name_id = 0, first = name.first, last = name.last, title = name.title),
    nat,
    phone,
    UserListData.Picture(pic_id = 0, picture = picture.large)
)

