package com.shadi.data.mapper

import com.shadi.data.api.ShadiApi
import com.shadi.domain.entity.Entity

/**
 * Created by Droider on 10/10/2018.
 */
/**
 * Extension class to map album dto to album entity
 */

fun ShadiApi.Dto.ShadiUsers.map() = Entity.Users(
    results.map { result ->
        Entity.Result(
            result.action,
            result.cell,
            result.email,
            result.gender,
            Entity.Name(result.name.first, result.name.last, result.name.title),
            result.nat,
            result.phone,
            Entity.Picture(result.picture.large)
        )

    }
)