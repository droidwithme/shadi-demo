package com.shadi.data.datasource.userlist

import android.util.Log
import com.shadi.data.db.userlist.UserListDao
import com.shadi.data.db.userlist.UserListData
import com.shadi.data.mapper.map
import com.shadi.domain.entity.Entity
import io.reactivex.Single
import java.util.concurrent.Executor

/**
 * Created by Droider on 9/28/2018.
 */
/**
 * Album database data source implementation
 */
class ShadiDatabaseDataSourceImpl(
    private val shadiListDao: UserListDao,
    private val ioExecutor: Executor
) : ShadiDatabaseDataSource {


    override fun getUsersFromDB(): Single<List<UserListData.UserData>> {
        return shadiListDao.getAllUser()
    }

    override fun saveUserToDb(listOfUser: Single<Entity.Users>) {
        Log.e("ShadiDatabaseSourceImpl", "saveUserToDb()")
        listOfUser.subscribe { t1 ->
            val listData: List<UserListData.UserData> = t1.results.map { it.map() }
            ioExecutor.execute {
                shadiListDao.insert(listData)
            }
        }
    }

    override fun updateUse(user: UserListData.UserData) {
        ioExecutor.execute {
            shadiListDao.update(user.email, user.action)
        }
    }

    override fun deleteDb() {
        ioExecutor.execute {
            shadiListDao.truncate()
        }
    }


}