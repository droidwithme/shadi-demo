package com.shadi.data.datasource.userlist

import com.shadi.data.datasource.BaseDataSource
import com.shadi.data.db.userlist.UserListData
import com.shadi.domain.entity.Entity
import io.reactivex.Single

/**
 * Created by Droider on 9/24/2018.
 */
/**
 * Album database data source
 */
interface ShadiDatabaseDataSource : BaseDataSource {

    fun getUsersFromDB(): Single<List<UserListData.UserData>>


    fun saveUserToDb(listOfUser: Single<Entity.Users>)

    fun updateUse(user: UserListData.UserData)

    fun deleteDb()
}