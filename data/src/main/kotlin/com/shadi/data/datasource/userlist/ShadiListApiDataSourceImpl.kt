package com.shadi.data.datasource.userlist

import io.reactivex.Single
import com.shadi.data.api.ShadiApi
import com.shadi.data.common.extension.applyIoScheduler
import com.shadi.domain.entity.Entity

/**
 * Created by Droider on 9/24/2018.
 */
/**
 * Album network data source implementation
 */
class ShadiListApiDataSourceImpl(private val api: ShadiApi) : ShadiListApiDataSource {
    override fun getShadiProfiles(page: Int): Single<Entity.Users> {
        return api.getUsers(page).applyIoScheduler().map { it ->
            Entity.Users(it.results.map {
                Entity.Result(
                    it.action,
                    it.cell,
                    it.email,
                    it.gender,
                    Entity.Name(it.name.first, it.name.last, it.name.title),
                    it.nat,
                    it.phone,
                    Entity.Picture(it.picture.large)
                )
            })
        }
    }
}
