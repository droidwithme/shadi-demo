package com.shadi.data.datasource.userlist

import android.annotation.SuppressLint
import android.util.Log
import io.reactivex.Single
import com.shadi.data.datasource.BaseDataSource
import com.shadi.domain.common.ResultState
import com.shadi.domain.entity.Entity

/**
 * Created by Droider on 9/24/2018.
 */


/**
 * network data source
 */
interface ShadiListApiDataSource : BaseDataSource {
    /**
     * Get all of albums from network
     */
    fun getShadiProfiles(page: Int): Single<Entity.Users>
}

@SuppressLint("CheckResult")
fun UserListApiDataSource(page: Int,
    apiSource: ShadiListApiDataSource,
    onResult: (result: ResultState<Entity.Users>) -> Unit
) {
    Log.e("LoginApiDataSource", "login()")
    apiSource.getShadiProfiles(page)
        .subscribe({ data ->
            Log.e("LoginApiDataSource", "apiSource.login($data)")
            onResult(ResultState.Success(data))
        }, { throwable ->
            Log.e("LoginApiDataSource", "apiSource.login($throwable)")
            onResult(ResultState.Error(throwable, null))
        })
}
