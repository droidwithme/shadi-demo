package com.shadi.data.db.userlist

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Droider on 10/12/2018.
 */
/**
 * Album data class
 */
sealed class UserListData {

    @Entity(tableName = "user_list")
    data class UserData(
        @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) val id: Long,
        var action: Boolean,
        var cell: String,
        var email: String,
        var gender: String,
        @Embedded
        var name: Name,
        var nat: String,
        var phone: String,
        @Embedded
        var picture: Picture
    ) : UserListData()

    @Entity(tableName = "user_names")
    data class Name(
        @ColumnInfo(name = "name_id") @PrimaryKey(autoGenerate = false) val name_id: Long,
        var first: String,
        var last: String,
        var title: String
    ) : UserListData()

    @Entity(tableName = "picture")
    data class Picture(
        @ColumnInfo(name = "pic_id") @PrimaryKey(autoGenerate = false) val pic_id: Long,
        var picture: String
    )

}
