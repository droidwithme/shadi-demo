package com.shadi.data.db.userlist

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shadi.data.db.BaseDao
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by Droider on 9/14/2018.
 */
/**
 * UserData dao
 */
@Dao
interface UserListDao : BaseDao<UserListData.UserData> {

    @Query("SELECT * FROM user_list WHERE id = :id")
    override fun select(id: Long): Flowable<UserListData.UserData>

    @Query("UPDATE user_list SET `action`=:action WHERE email = :email")
    fun update(email: String?, action: Boolean)

    @Query("SELECT * FROM user_list")
    fun getAllUser(): Single<List<UserListData.UserData>>

    @Query("SELECT * FROM user_list ORDER BY id")
    override fun selectAllPaged(): DataSource.Factory<Int, UserListData.UserData>

    @Query("DELETE FROM user_list")
    override fun truncate()


}