package com.shadi.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shadi.data.db.userlist.UserListDao
import com.shadi.data.db.userlist.UserListData

/**
 * Created by Droider on 9/14/2018.
 */
/**
 * Database class with all of its dao classes
 */
@Database(
    entities = [UserListData.UserData::class,UserListData.Name::class],
    version = 1,
    exportSchema = false
)
abstract class MyDatabase : RoomDatabase() {



    abstract fun userDao(): UserListDao
}