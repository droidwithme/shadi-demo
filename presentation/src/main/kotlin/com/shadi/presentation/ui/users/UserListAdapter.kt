package com.shadi.presentation.ui.users

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.shadi.domain.entity.Entity
import com.shadi.presentation.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_user_list.view.*


class UserListAdapter(var dataList: ArrayList<Entity.Result>) :
    RecyclerView.Adapter<UserListAdapter.MyViewHolder>() {

    private val onUserClickItem = PublishSubject.create<Entity.Result>()

    val userClickEvent: Observable<Entity.Result> = onUserClickItem


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var userName = view.userName
        var userEmail = view.userEmail
        var userGender = view.userGender
        var userImage = view.userImage
        var buttonAction = view.buttonAcceptDecline
    }

    fun submitData(data: List<Entity.Result>) {
        this.dataList.clear()
        this.dataList.addAll(data)
        notifyDataSetChanged()
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_user_list, parent, false)
        return MyViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.userName.text = "${data.name.first} ${data.name.last}"
        holder.userEmail.text = data.email
        holder.userGender.text = data.gender
        Picasso.get()
            .load(data.picture.large)
            .into(holder.userImage, object : Callback {
                override fun onSuccess() {}
                override fun onError(e: Exception) {}
            })
        if (data.action) {
            holder.buttonAction.text = "Decline"
        } else {
            holder.buttonAction.text = "Accept"
        }
        holder.buttonAction.setOnClickListener {
            data.action = !data.action
            notifyItemChanged(position)
            data.let {
                val product: Entity.Result = data
                onUserClickItem.onNext(product)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}