package com.shadi.presentation.ui.users

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.shadi.domain.common.ResultState
import com.shadi.domain.entity.Entity
import com.shadi.domain.usecase.users.UserListUseCase
import com.shadi.presentation.common.OperationLiveData
import com.shadi.presentation.ui.base.BaseViewModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 * Created by Droider on 9/21/2018.
 */
class UsersViewModel @Inject constructor(private val userListUseCase: UserListUseCase) :
    BaseViewModel() {

    val TAG = UsersViewModel::class.simpleName
    private var tempDispossable: Disposable? = null


    private val fetch = MutableLiveData<String>()

    val userListLiveData: LiveData<ResultState<List<Entity.Result>>> =
        Transformations.switchMap(fetch) {
            OperationLiveData<ResultState<List<Entity.Result>>> {
                if (tempDispossable?.isDisposed != true)
                    tempDispossable?.dispose()
                tempDispossable = userListUseCase.getUserList().subscribe { resultState ->
                    Log.e(TAG, "data received in view model ${resultState}")
                    postValue((resultState))
                }
                tempDispossable?.track()
            }
        }

    fun getUserList() {
        fetch.postValue("")
    }


    private val offlineData = MutableLiveData<String>()

    val offlineUsers: LiveData<ResultState<List<Entity.Result>>> =
        Transformations.switchMap(offlineData) {
            OperationLiveData<ResultState<List<Entity.Result>>> {
                if (tempDispossable?.isDisposed != true)
                    tempDispossable?.dispose()
                tempDispossable = userListUseCase.getOfflineData().subscribe { resultState ->
                    Log.e(TAG, "offline data ${resultState}")
                    postValue((resultState))
                }
                tempDispossable?.track()
            }
        }


    fun getOfflineUser() {
        offlineData.postValue("")
    }


    private val userToBeUpdate = MutableLiveData<Entity.Result>()

    val updateResponse: LiveData<String> =
        Transformations.switchMap(userToBeUpdate) {
            OperationLiveData<String> {
                if (tempDispossable?.isDisposed != true)
                    tempDispossable?.dispose()
                tempDispossable = userListUseCase.updateUser(it).subscribe { resultState ->
                    Log.e(TAG, "data received in view model ${resultState}")
                    postValue((resultState))
                }
                tempDispossable?.track()
            }
        }


    fun updateUser(user: Entity.Result) {
        userToBeUpdate.postValue(user)
    }

}