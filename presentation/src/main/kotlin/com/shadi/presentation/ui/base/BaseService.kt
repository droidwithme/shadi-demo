package com.shadi.presentation.ui.base

import dagger.android.DaggerService

/**
 * Created by Droider on 10/15/2018.
 */
abstract class BaseService : DaggerService()