package com.shadi.presentation.ui.users

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.shadi.domain.common.ResultState
import com.shadi.domain.entity.Entity
import com.shadi.presentation.R
import com.shadi.presentation.common.extension.applyIoScheduler
import com.shadi.presentation.common.extension.observe
import com.shadi.presentation.ui.MainActivity
import com.shadi.presentation.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_user_list.*
import javax.inject.Inject
import android.net.NetworkInfo

import androidx.core.content.ContextCompat.getSystemService

import android.net.ConnectivityManager
import androidx.core.content.ContextCompat


/**
 * Created by Droider.
 */
class UsersFragment : BaseFragment() {
    val TAG = UsersFragment::class.simpleName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var isLoading = false

    var adapter = UserListAdapter(ArrayList())

    private val viewModel: UsersViewModel by lazy {
        logMessage(TAG!!, "providing view model")
        ViewModelProviders.of(this, viewModelFactory).get(UsersViewModel::class.java)
    }

    private fun onAlbumDeleted(resultState: ResultState<String>) {
        logMessage(TAG!!, "onAlbumDeleted")
        when (resultState) {
            is ResultState.Success -> Toast.makeText(activity, resultState.data, Toast.LENGTH_SHORT)
                .show()
            is ResultState.Error -> Toast.makeText(
                activity,
                resultState.throwable.message,
                Toast.LENGTH_SHORT
            ).show()
            else -> {
                Toast.makeText(activity, "Something happened unexpected", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun userListLiveData(liveUserList: ResultState<List<Entity.Result>>) {
        logMessage(TAG!!, "userListLiveData($liveUserList)")
        swipeToRefresh.isRefreshing = false
        when (liveUserList) {
            is ResultState.Success -> {
                hideLoading()
                adapter.submitData(liveUserList.data)
            }
            is ResultState.Error -> {
                hideLoading()
                if (liveUserList.throwable.message === "Unable to resolve host \"randomuser.me\": No address associated with hostname") {
                    Toast.makeText(
                        context,
                        "Seems you do not have an active internet connection, Fetching users from database",
                        Toast.LENGTH_LONG
                    ).show()
                }
                viewModel.getOfflineUser()
            }
            is ResultState.Loading -> {
                adapter.submitData(liveUserList.data)
            }
        }
        isLoading = false
    }

    private fun offLineListLiveData(userList: ResultState<List<Entity.Result>>) {
        logMessage(TAG!!, "offListLiveData($userList)")
        swipeToRefresh.isRefreshing = false
        when (userList) {
            is ResultState.Success -> {
                hideLoading()
                if (userList.data.isEmpty()) {
                    Toast.makeText(
                        context,
                        "Seems you do not have user data in database as well, Please turn on the internet and try again.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                adapter.submitData(userList.data)
            }
            is ResultState.Error -> {
                hideLoading()
                Toast.makeText(activity, userList.throwable.message, Toast.LENGTH_SHORT).show()
                adapter.submitData(ArrayList())
            }
            is ResultState.Loading -> {
                adapter.submitData(userList.data)
            }
        }
        isLoading = false
    }

    private fun updateUser(updateMsg: String) {
        logMessage(TAG!!, "updateUser($updateMsg)")
        Toast.makeText(context, updateMsg, Toast.LENGTH_LONG).show()
    }

    @SuppressLint("CheckResult")
    private fun initView() {
        logMessage(TAG!!, "initView()")
        recyclerViewUserList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerViewUserList.setHasFixedSize(true)
        recyclerViewUserList.adapter = adapter

        adapter.userClickEvent.applyIoScheduler().subscribe { it ->
            logMessage(TAG, "applyToScheduler")
            viewModel.updateUser(it)
        }

        swipeToRefresh.setOnRefreshListener {
            if (isNetworkAvailable(context!!)) {
                viewModel.getUserList()
            } else {
                viewModel.getOfflineUser()
            }
        }

        showLoading()
    }

    override fun onStart() {
        super.onStart()
        logMessage(TAG!!, "onStart()")
        (activity as MainActivity).showToolBar("List Of Users")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logMessage(TAG!!, "onCreateView()")
        return inflater.inflate(R.layout.fragment_user_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logMessage(TAG!!, "onViewCreated()")
        initView()
        observe(viewModel.userListLiveData, ::userListLiveData)
        observe(viewModel.offlineUsers, ::offLineListLiveData)
        observe(viewModel.updateResponse, ::updateUser)
        if (isNetworkAvailable(context!!)) {
            viewModel.getUserList()
        } else {
            viewModel.getOfflineUser()
        }


    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}